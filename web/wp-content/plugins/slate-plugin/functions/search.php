<?php

//Helps incorporate ACF fields into building excerpts for searches and RSS
add_filter('relevanssi_excerpt_content', 'frn_custom_fields_to_content', 10, 3);
function frn_custom_fields_to_content($content, $post, $query) {

    while(the_flexible_field("flexible-content")) {

        // Spotlight
        if(get_row_layout() == "spotlight_clone") {
            $content .= " " . get_sub_field("card_header");
            $content .= " " . get_sub_field("card_secondary_text");
        }

        // Deck
        elseif(get_row_layout() == "deck_clone") {
            $content .= " " . get_sub_field("card_header");
            $content .= " " . get_sub_field("card_secondary_text");
        }
                   
        elseif(get_row_layout() == "two_columns_content") {
                $content .= " " . get_sub_field("first_description");
                $content .= " " . get_sub_field("second_description");
        }

    }

  return $content;
}









