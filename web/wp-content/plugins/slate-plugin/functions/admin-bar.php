<?php

// Add a custom Site Title to the Dashboard Toolbar
function my_admin_bar() {
	global $wp_admin_bar;
	
	$args = array(
        'id'     => 'wp-logo',                         // id of the existing child node (New > Post)
        'title'  => __( get_bloginfo('name'), 'textdomain' ), // alter the title of existing node
        'parent' => false,                              // set parent to false to make it a top level (parent) node
    );

    $sitename = array(
        'id'     => 'site-name',                         // id of the existing child node (New > Post)
        'title'  => __( get_bloginfo('name'), 'textdomain' ), // alter the title of existing node
        'parent' => false,                              // set parent to false to make it a top level (parent) node
        'meta' => array( 'target' => site_url() ), // array of any of the following options: array( 'html' => '', 'class' => '', 'onclick' => '', target => '', title => '' );
    );
    $visitsite = array(
        'id'     => 'wp-ben-stupid',                         // id of the existing child node (New > Post)
        'title'  => 'bens fb',
        'parent' => 'wp-logo',                              // set parent to false to make it a top level (parent) node
        'meta' => array( 'target' => 'blank' ), // array of any of the following options: array( 'html' => '', 'class' => '', 'onclick' => '', target => '', title => '' );
        'href' => 'https://www.facebook.com/benjamin.wrong',
    );

	//Add a link called 'My Link'...
	$wp_admin_bar->add_node($args);
	$wp_admin_bar->add_node($sitename);
  $wp_admin_bar->add_node($visitsite);
	$wp_admin_bar->remove_menu('view-site');
  $wp_admin_bar->remove_menu('wp-logo-external');
}
add_action( 'wp_before_admin_bar_render', 'my_admin_bar', 999 ); 



