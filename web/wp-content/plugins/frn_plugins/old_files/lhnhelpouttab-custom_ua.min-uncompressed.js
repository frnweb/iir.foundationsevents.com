
//frn version 3.0
//frn version 3.1: added phone number as label in GA event trigger for mobile phone clicks
//frn version 3.2: added Hotjar event tracking capability
//frn version 3.3: removed UA option, and remove any chat Google Events tracking

if(typeof frn_hj_act === 'undefined') var frn_hj_act="";

//The following adds custom text below the two buttons in slide out
function LHN_HelpPanel_onLoad(){
    
  //Best for PPC rehab & intervention custom phone numbers per page. But also a good backup in case it's undefined in the head area.
  //If no phone number in head area...
  if(typeof frn_phone === 'undefined') frn_phone="";
  if(typeof frn_phoneBasic === 'undefined') frn_phoneBasic="";  //this var created by other javascript found on the page. Only available when JS is used to identify numbers.
  if(frn_phone===null) frn_phone=""; //activate variable
  if(frn_phone==="") {
    //if frn blank, see if Basic is defined. If so, set frn_phone with the number for slideout.
    if(frn_phoneBasic!=="") {
      frn_phoneBasic=frn_phoneBasic.trim();
      if(frn_phoneBasic!=="") frn_phone=frn_phoneBasic;
    }
  }
  
  //var pageURL = window.location.href;
  if(frn_phone.length>19) frn_phone="";
  
  //format the phone number if it has no formatting and create link code if mobile device
  if(frn_phone!=="") {
    frn_phone= frn_phone.trim();
    frn_phone = frn_phone.replace(/\D/g,'');  //removed -- can't remember why it was used at beggin of this line if(frn_phoneBasic!=="") 
    if(frn_phone.length==11) {frn_phoneFormat = /^1?(\d)([0-9]..)([0-9]..)([0-9]...)$/; //starts with 1
      frn_phone = frn_phone.replace(frn_phoneFormat,'$1 ($2) $3-$4');}
    else if(frn_phone.length==10) {frn_phoneFormat = /([0-9]..)([0-9]..)([0-9]...)$/; //doesn't start with 1
      frn_phone = frn_phone.replace(frn_phoneFormat,'($1) $2-$3');}
    else if(frn_phone.length==13){frn_phoneFormat = /([0-9]..)([0-9]..)([0-9]..)([0-9]...)$/; //international
      frn_phone = frn_phone.replace(frn_phoneFormat,'$1 ($2) $3-$4');}
    else if(frn_phone.length>=14 && frn_phone.slice(0,1)!="("){frn_phoneFormat = /([0-9]...)([0-9]..)([0-9]..)([0-9]...)$/; //international
      frn_phone = frn_phone.replace(frn_phoneFormat,'$1 ($2) $3-$4');}
    //Set link code for mobile devices
    if((typeof isTierIphone !== 'undefined') || (typeof isTierTablet != 'undefined')) {
      if(frn_phoneBasic==="") frn_phoneBasic = frn_phone.replace(/\D/g,''); //strips out any non-numbers
      frn_phone = '<a href="tel:'+frn_phoneBasic+'">'+frn_phone+'</a>';
    }
  }
  
  //Set custom text below phone number
    if(frn_customText.trim()==="") frn_customText='<span style="white-space:nowrap;"><p class="flyout-phone">' + frn_phone + '</p><p class="flyout-bottom">Confidential & Private</p></span>'; 
    
    //Create DIV with the content - used for GA event tracking
    frn_addphone = document.createElement('div');
    frn_addphone.class = 'lhn_live_phone';
    frn_addphone.innerHTML = frn_customText;
    
    //Find the DIV with the class that content needs to be added within
    var classObjs = document.getElementsByTagName('div'), i; //store all divs in page
    for (i in classObjs) {
        if(typeof classObjs[i].className === 'undefined') "";
    else {
        if((classObjs[i].className).indexOf('lhn_btn_container') > -1) {
              //alert('lhn_btn_container was found!');
          //if it doesn't find "a div with lhn_btn_container, then it will error out;
              classObjs[i].appendChild(frn_addphone);
          }
    }
    }
  }
  
//Tracks clicks/events on LiveHelpNow assets in Analytics
function wireCustomLHNEvents() {
   
  //Helpout frequently used elements
  window.lhn("#lhn_help_btn").click(function(){  //initial help out tab button
      //lhnPushAnalytics('Live Chat', 'Contact Us Slide Out');
      if(frn_hj_act!=="") {hj('vpv', '/lhnchat-window-click-for-slide-out/'); } //alert("hotjar tracked");
  });
  
  window.lhn("#lhn_helppanel .lhn_live_chat_btn").click(function(){ //chat button
      //lhnPushAnalytics('Live Chat', 'Contact Us Slide Out: Chat Button', 'Chats');
      if(frn_hj_act!=="") hj('vpv', '/lhnchat-window-click-for-help-panel/');
  });
  window.lhn("#lhn_helppanel .lhn_ticket_btn").click(function(){  //email button
      //lhnPushAnalytics('Live Chat', 'Contact Us Slide Out: Email Button', 'Emails');
      if(frn_hj_act!=="") hj('vpv', '/lhnchat-window-click-on-email-button/');
  });
  if((typeof isTierIphone !== 'undefined') || (typeof isTierTablet != 'undefined')) {
      window.lhn("#lhn_helppanel .flyout-phone").click(function(){  //Phone number clicks
          lhnPushAnalytics('Live Chat', 'Contact Us Slide Out: Phone Number', frn_phoneBasic);
          if(frn_hj_act!=="") hj('vpv', '/lhnchat-window-click-on-phone-number/');
      });
   }
   
  //On-page buttons
  window.lhn("img#lhnchatimg").click(function(){  //Chat button
    lhnPushAnalytics('Live Chat', 'Custom Buttons: Chat Clicks', 'Chats');
    if(frn_hj_act!=="") hj('vpv', '/lhnchat-onpage-click-on-chat-button/');
  });
  window.lhn("#lhnEmailButton").click(function(){  //Email button
    lhnPushAnalytics('Live Chat', 'Custom Buttons: Email Clicks', 'Emails');
    if(frn_hj_act!=="") hj('vpv', '/lhnchat-onpage-click-on-email-button/');
  });
   
  //Close Windows
  window.lhn("#lhn_helppanel #lhn_help_exit").click(function(){
    lhnPushAnalytics('Live Chat', 'Contact Us Slide Out: Close Button');
    if(frn_hj_act!=="") hj('vpv', '/lhnchat-window-click-close-button/');
  });

  //Less Used Options
  window.lhn("#lhn_helppanel .lhn_options_btn").click(function(){
    lhnPushAnalytics('Live Chat', 'Contact Us Slide Out: More Options');
    if(frn_hj_act!=="") hj('vpv', '/lhnchat-window-click-more-options/');
  });
  /* 
  //The following we have disabled in our LHN account. No need to have browser keep looking for these things.
  window.lhn("#lhn_helppanel #lhn_search_box").blur(function(){
    if(window.lhn(this).val() !== ""){
     lhnCurVal = window.lhn(this).val();
     if(typeof(lhnOldVal) === 'undefined' || lhnOldVal != lhnCurVal){
      lhnPushAnalytics('Live Chat', 'Contact Us Slide Out: Search', encodeURIComponent(window.lhn(this).val()));
      lhnOldVal = lhnCurVal;
     }
    }
  });
  window.lhn("#lhn_helppanel .lhn_callback_btn").click(function(){
    //lhnPushAnalytics('Live Chat', 'Contact Us Slide Out: Callback Button');
    if(frn_hj_act!=="") hj('vpv', '/lhnchat-window-click-for-callback/');
   });
  */
}

//Function for reporting events. Check first to make sure it's not defined in the LHN JS already.
if (typeof lhnPushAnalytics === 'undefined' ) {
  //alert("It's not already defined");
  ga_count = 1;
  function lhnPushAnalytics(category, event, label){
    //if(typeof(ga) !== 'undefined') {
      //Old: _gaq.push(['_trackEvent', category, event, label]);
      ga('send', 'event', category, event, label);
      //alert("lhnPushAnalytics has been ran "+ga_count+" times. Category: "+category+ "; Event: "+event+ "; Label:" +label);
      ga_count=ga_count+1;
    //}
  }
}
//else alert("It is already defined");
  

