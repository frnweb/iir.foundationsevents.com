function showhide(layer_id,action) {
	//alert(action);
	if(typeof action == 'undefined') action="";
	if(action!=="") document.getElementById(layer_id).style.display=action; 
	else {
		var show_object = document.getElementById(layer_id).style.display;
		if(show_object=="none" || show_object=="") document.getElementById(layer_id).style.display="block";
		else document.getElementById(layer_id).style.display="none";
	}
}
	
function selectText(containerid) {
	if (document.selection) {
		var range = document.body.createTextRange();
		range.moveToElementText(document.getElementById(containerid));
		range.select();
	} else if (window.getSelection) {
		var range = document.createRange();
		range.selectNode(document.getElementById(containerid));
		window.getSelection().addRange(range);
	}
}

function disable_ga_features(input,input2) {
	//disable and uncheck both inputs
	var field=document.getElementById(input);
	field.disabled = true;
	field.checked = false;
	var field2=document.getElementById(input2);
	field2.disabled = true;
	field2.checked = true;
}

function enable_ga_features(input,input2) {
	//disable and uncheck both inputs
	var field=document.getElementById(input);
	field.disabled = false;
	var field2=document.getElementById(input2);
	field2.disabled = false;
}

function add_keypages_fields(root_id,kp,remove) {
   var d = document.getElementById(root_id);
   //remember that the first DIV in this root_id is the column headers. We need to remember to skip them when calculating numbers.
	
   if(remove=="delete") {
		d.removeChild(d.children[kp]); //since the field headers are inside the root_id area, we don't need to minus 1 like we do for the larger boxes
		var input_array = d.getElementsByTagName('input');
		var total = input_array.length;
		for (var i=0; i<total; i++) {
			input_array[i].setAttribute('value',input_array[i].value);
			//alert(input_array[i].value);
			if((i+1)==total) {
				var lastrow = d.children[kp-1].innerHTML;
				d.children[kp-1].innerHTML = lastrow; //.replace('<!--[+]-->','[+]'); //activate the last plus sign
			}
		}
   }
   else {
		
		var input_array = d.getElementsByTagName('input');
		var total = input_array.length;
		for (var i=0; i<total; i++) {
			input_array[i].setAttribute('value',input_array[i].value);
			//alert(input_array[i].value);
		}
		
		//d.innerHTML = d.innerHTML.replace('>[+]<','><!--[+]--><'); //deactivate all plus signs 
		
		//create new node that'll be added later
		var new_row = document.createElement("div"); 
		new_row.setAttribute('id',root_id+"-"+(kp+1));
		//make old fields the new fields
		var d_keypage = document.getElementById(root_id+"-"+kp);
		var new_row_HTML = d_keypage.innerHTML;
		new_row.innerHTML = new_row_HTML; //.replace('<!--[+]-->','[+]'); //activate the last plus sign
		//clear values
		input_array = new_row.getElementsByTagName('input');
		total = input_array.length;
		for (i=0; i<total; i++) {
			//alert(input_array[i].value);
			input_array[i].setAttribute('value','');
		}
		d.appendChild(new_row); //do this before clearing out inputs
   }
   
	//redo all numbered elements so they stay sequential before saving
	//we do this since they could be copying any fields in the list
	total=d.children.length; //getting total once more
	kp_number=1; var kp_number_for_saving=0;
	for (var i=0; i<total; i++) {
	   
	   var kps_HTML = d.children[i].innerHTML;
	   //skip the first child since it's the column headers
		if(i!=0) {
			d.children[i].setAttribute("id", root_id+'-'+kp_number);
			// change classes/field IDs
			//alert(/_\d\"\s/g.exec(kps_HTML));
			kps_HTML = kps_HTML.replace('id="'+root_id+'-'+kp,'id="'+root_id+'-'+(kp_number)); //change row ID
			kps_HTML = kps_HTML.replace(/\]\[key_pages\]\[\d+\]/g,'][key_pages]['+(kp_number_for_saving)+']'); //change field saving name
			kps_HTML = kps_HTML.replace(/,\d+,/g,','+(kp_number)+','); //change saving number in javascript for deleting rows
			kps_HTML = kps_HTML.replace(/,\d+\)/g,','+(kp_number)+')'); //change current row number for adding a row in javascript
			kps_HTML = kps_HTML.replace(/\'\d+\,\'delete\'/,','+kp_number+',\'delete'); //change current row number for deleting a row in javascript
			kps_HTML = kps_HTML.replace(/\d\.\s/,kp_number+'. '); //change number displayed on page
			
			d.children[i].innerHTML = kps_HTML;
			kp_number++;
			kp_number_for_saving++;
		}
	}
   
}

function remove_blocks(new_type,box_id,prev_type,dd_switches,default_pos_cta,default_pos_kps,default_pos_msg) {
	
	//IDs for sections:
	var cta_id = "cta_"+box_id;
	var kps_id = "key_pages_"+box_id;
	var other_block = "";
	var its_block = "";
	var dd_id_base = "frn_personal_";
	var dd_values_chg = dd_switches.split(",");
	var trigger_note = document.getElementById('frn_personal_triggers_note_'+box_id);
	var position_div = "";
	var position_HTML = "";
	
	if(new_type=="") new_type="cta";
	
	if(new_type=="cta") {
		its_block = document.getElementById(cta_id);
		its_block.style.display="block";
		if(typeof previous_cta_innerHTML !== 'undefined') {
			//if cta was already removed, we need to add it back now
			if(previous_cta_innerHTML!=="") {
				its_block.innerHTML = previous_cta_innerHTML;
			}
		}
		//There's a little note about CTA trigger word defaults for rehab pages that needs to be displayed if CTA is in the dropdown.
		trigger_note.style.display="block";

		//remove key pages block
		other_block = document.getElementById(kps_id);
		//assumes that if it's block, then it's showing and has content
		if(other_block) {
		if(other_block.style.display=="block") {
			previous_kps_innerHTML = other_block.innerHTML;  //automatically a global variable we can all on next run based on selection
			other_block.innerHTML = "";
			other_block.style.display="none";
		}
		}
	}
	else if(new_type=="kps") {
		its_block = document.getElementById(kps_id);
		//alert(kps_id);
		its_block.style.display="block";
		if(typeof previous_kps_innerHTML !== 'undefined') {
			//if key pages was already removed, we need to add it back now
			if(previous_kps_innerHTML!=="") {
				its_block.innerHTML = previous_kps_innerHTML;
			}
		}

		//remove cta block
		other_block = document.getElementById(cta_id);
		//assumes that if it's block, then it's showing and has content
		if(other_block) {
		if(other_block.style.display=="block") {
			previous_cta_innerHTML = other_block.innerHTML;  //automatically a global variable we can all on next run based on selection
			other_block.innerHTML = "";
			other_block.style.display="none";
			//There's a little note about CTA trigger word defaults for rehab pages that needs to be hidden if CTA isn't in the dropdown.
			trigger_note.style.display="none";
		}
		}
	}
	else if(new_type=="msg") {
		//remove cta block
		var cta_block = document.getElementById(cta_id);
		//assumes that if it's block, then it's showing and has content
		if(cta_block.style.display=="block") {
			var cta_innerHTML = cta_block.innerHTML;
			previous_cta_innerHTML = cta_innerHTML;
			cta_block.innerHTML = "";
			cta_block.style.display="none";
			trigger_note.style.display="none";
		}

		//remove kps block
		var kps_block = document.getElementById(kps_id);
		//assumes that if it's block, then it's showing and has content
		if(kps_block.style.display=="block") {
			var kps_innerHTML = kps_block.innerHTML;
			previous_kps_innerHTML = kps_innerHTML;
			kps_block.innerHTML = "";
			kps_block.style.display="none";
			//There's a little note about CTA trigger word defaults for rehab pages that needs to be hidden if CTA isn't in the dropdown.
			trigger_note.style.display="none";
		}
	}
	
	
	
	//change default new_type percent in position dropdown
	new_type=new_type.toUpperCase();
	if(typeof saved_type !== 'undefined') prev_type=saved_type; //this.options[this.selectedIndex].value;
	prev_type=prev_type.toUpperCase();
	var pattern = ""; var replace_default="";
	var total_dd=dd_values_chg.length;
	
	for (var dd=0; dd<total_dd; dd++) {
		div = document.getElementById(dd_id_base+dd_values_chg[dd]+"_"+(box_id));
		HTML = div.innerHTML;
		//alert(new_type + " " + prev_type);
		if(dd_values_chg[dd]=="position") {
			pattern = new RegExp(prev_type+' Default \\(\\d+%\\)','g');
			if(new_type=="KPS") default_pos=default_pos_kps;
				else if(new_type=="KPS") default_pos=default_pos_msg;
				else default_pos=default_pos_cta;
			HTML = HTML.replace(pattern , new_type+" Default ("+default_pos+")");
		}
		else {
			pattern = new RegExp(prev_type+' Default \\((?:.+)\\)','g');
			if(new_type=="KPS") replace_default = "End of Sections";
			else replace_default = "No Restrictions";
			HTML = HTML.replace(pattern , new_type+" Default ("+replace_default+")");
		}
		//position_HTML = position_HTML.replace(/MSG Default \(\d+%\)/,new_type+" Default ("+default_pos_cta+")");
		div.innerHTML = HTML;
	}
	saved_type=new_type;
	
	
}

function add_contact_boxes(parent_id,root_id,box_number,remove) {
   //root_id is only the base ID value so we can add a number as appropriate

   var box_template_HTML;
   var input_array;
   var total=0;
   
   if(parent_id) {
	   
	   var d = document.getElementById(parent_id);
	   
	   if(remove=="delete") {
		   //removes individual box
		   d.removeChild(d.children[box_number-1]); 
	   }
	   else {
			//this section adds a new box to the end since we don't want to remove one

			/*
			//This didn't work consistently. Gave up until we want to try again.
			//replace any input values with the new ones if there are any
			input_array = d.getElementsByTagName('input');
			total = input_array.length;
			for (var i=0; i<total; i++) {
				//if radio input, check for checked state
				if(input_array[i].getAttribute('type')=="radio") {
					if(input_array[i].getAttribute('checked')) input_array[i].setAttribute('checked','checked');
				}
				else input_array[i].setAttribute('value',input_array[i].value);
				//alert(input_array[i].value);
			}
			*/
			
			//get parent DIV & get last box innerHTML
			var current_block = d.innerHTML;
			var current_box = document.getElementById(root_id+"_"+box_number);
			box_template_HTML = current_box.innerHTML;
			var box_number_for_saving = (box_number-1);
			
			//remove all selected options in dropdowns
			box_template_HTML = box_template_HTML.replace(' selected="selected"','');
			box_template_HTML = box_template_HTML.replace(/ checked/,'');
			box_template_HTML = box_template_HTML.replace(/display: block/g,'display: none');
			style="display: block;"
			//alert(/ checked/.exec(box_template_HTML));
			
			
			/*
			//This didn't work consistently. Gave up until we want to try again.
			//Now add back the option that is currently selected (must load all selects and when selected option is found, add the select HTML)
			//template: box_template_HTML = box_template_HTML.replace("old", "new");
			var select_array = current_box.getElementsByTagName('select');
			total = select_array.length;
			for (var i = 0; i < total; i++) {
				//since nothing is selected in the code itself, we can search by the value and add it
				box_template_HTML = box_template_HTML.replace('value="'+select_array[i].value+'"','value="'+select_array[i].value+'" selected="selected"');
				//other approach: e.options[e.selectedIndex].value;
			}
			*/
			
			//strip all input values
			total=0;
			var input_array = current_box.getElementsByTagName('input');
			total = input_array.length;
			for (var i = 0; i < total; i++) {
				//don't remove value from a radio input since they need to stay set
				if(input_array[i].getAttribute('type')!=="radio") {
					//box_template_HTML = box_template_HTML.replace('value="'+input_array[i].value+'"','value=""');
				}
			}
			
			//strip all textarea values
			
			total=0;
			var textarea_array = current_box.getElementsByTagName('textarea');
			total = textarea_array.length;
			for (var i = 0; i < total; i++) {
					//box_template_HTML = box_template_HTML.replace('>'+textarea_array[i].value+'<','><');
			}
			
			box_template_HTML = box_template_HTML.replace(/frn_personal_boxes\[boxes\]\[\d+\]\[/g,'frn_personal_boxes[boxes][TBD_'+box_number_for_saving+'][');
			box_template_HTML = box_template_HTML.replace(/frn_personal_boxes\[boxes\]\[TBD_\d+\]\[/g,'frn_personal_boxes[boxes][TBD_'+box_number_for_saving+'][');
			//box_template_HTML = box_template_HTML.replace(/frn_personal_box\'\,\d+\);/g,'frn_personal_box\','+(box_number+2)+');'); //replaces box number in box add/delete options to the new box's ID in case someone removes it
			//box_template_HTML = box_template_HTML.replace(/frn_personal_keypages_(\d+)\'\,\d+\);/g,'frn_personal_keypages_$1\',TBD_'+box_number_for_saving+');'); //replaces box number in key pages javascript (so that input names are changed and don't override other boxes if a key page link is added)
			box_template_HTML = box_template_HTML.replace(/ID\:\d+/g,'ID: [TBD]');

			//This adds it back to the page
			d.innerHTML = current_block+'\
			<div id="'+root_id+'_'+(box_number+1)+'" class="frn_personal_box" >\
				'+box_template_HTML+'\
			</div>\
			';
			
			total=0;
			box_template_HTML="";
	   }

		
		//PROBLEM AT THIS STAGE: 
			//The problem is that if a box is removed or added, visually on the screen or in the HTML, there could be duplication.
			//To keep from needing to track where numbers are now, we can just reorder everything sequentially (except official box IDs)
		
		//FOR EACH BOX:
			//renumber all HTML ids with numbers in them to be sequential starting from 1 for ids
			//Problem: This requires us to change the HTML on the page. Any manually changed values in fields before saving will be lost when we reprint the HTML back onto the page.
			//Solution: Use JS to get the field values and force change the HTML in the page before printing back onto the page
		
		//Get all boxes within the root_id
		var all_boxes = d.getElementsByClassName(root_id);
		//total = all_boxes.length;
		//alert("boxes: " + total);
		
		
		//Loop through them and change numbers sequentially
		box_number=0; //var box_number_for_saving=0;
		for (var k=0; k<all_boxes.length; k++) {
		   //alert("box #" + k);
		   
		   //Get inner HTML for each box
			var box_HTML = all_boxes[k].innerHTML;

			
											/*
											//Decided to make the minus sign active for all
											//Activate minus signs if more than one
											if(total>1) box_HTML = box_HTML.replace('<!--&#8211;-->','&#8211;'); // activate all delete buttons
												else box_HTML = box_HTML.replace('box">&#8211;','box"><!--&#8211;-->'); // else since only one, remove delete option				
											*/
											
											/*
											//Removed this since it was causing issues. Couldn't figure a way to make it work.
											//Keeping code in case it helps in the future.
											//Solidify latest Textarea values (it's redundant if values 
											var textarea_array = all_boxes[k].getElementsByTagName('textarea');
											//alert("total textarea: "+textarea_array.length);
											for (var i=0; i<textarea_array.length; i++) {
												//alert("textarea name: "+textarea_array[i].getAttribute('name')+"; value: "+textarea_array[i].value+ "; HTML: " +textarea_array[i].innerHTML);
												if(textarea_array[i].value !== textarea_array[i].innerHTML) {
													pattern = />(?:.*)<\/textarea>/i;
													//alert(pattern.exec(box_HTML));
													box_HTML = box_HTML.replace(pattern,'>'+textarea_array[i].value+'</textarea>');
													//alert("text area value changed");
												}
											}
											
											total=0;
											var input_array = all_boxes[k].getElementsByTagName('input');
											//alert("total textarea: "+input_array.length);
											for (var i=0; i<input_array.length; i++) {
												//alert("input name: "+input_array[i].getAttribute('name')+"; value: "+input_array[i].value+ "; HTML: " +input_array[i].innerHTML);
												if(input_array[i].value !== input_array[i].getAttribute('value')) {
													pattern = new RegExp('value=\"'+input_array[i].getAttribute('value'));
													//alert(pattern.exec(box_HTML));
													box_HTML = box_HTML.replace(pattern,'value="'+input_array[i].value+'');
													alert("new value for "+input_array[i].getAttribute('name')+": " + input_array[i].value);
												}
											}
											*/
			
			
			// Increment HTML DIVs and JS numbers up one
			// IMPORTANT: 
			//		We cannot increment numbers in brackets ([]). Those numbers are in field names and need to stay unique to the box so that manually entered shortcodes continue to work whether or not some are deleted or added.
			// 		Input box IDs are assigned upon saving and managed at that point.
			//		To make sure Box IDs aren't changed in field names, the only thing we change when adding a box is making the old [#] into [TBD_#]. The PHP side of things changes it to the next highest number.
			
			//Change # on page
			box_HTML = box_HTML.replace(/\"\>\d+\</g,'">'+(box_number+1)+'<'); //change message number displayed on page
			//shows what regex finds: alert(/_\d\"\s/g.exec(box_HTML));
			box_HTML = box_HTML.replace(/_\d+/g,'_'+(box_number)+''); // all classes and IDs
			box_HTML = box_HTML.replace(/this\.value\,\d+\,/g,'this.value,'+(box_number)+',');  //for type dropdown javascript to change the number in the remove blocks call to hide/show kps fields or cta
			box_HTML = box_HTML.replace(/frn_personal_box\',\d+/g,"frn_personal_box',"+(box_number+1)); // all classes and IDs in javascript calls
			
			all_boxes[k].setAttribute('id',root_id+"_"+(box_number+1));
			
			
			//Put it all back into the page
			all_boxes[k].innerHTML = box_HTML;
			box_number++; total=0;

	   }

   }
}