<?php


/*
limits:
Will not search custom fields
Will not search plugins or theme settings
Only searches published posts
Limited to PHP files only
*/


function frn_shortcodes_admin() {

	if(is_admin()) { //just making sure this more intensive action doesn't happen for regular page loads.
		?>

		<p>The following information tells you if any shortcode created by this plugin is used in posts or PHP files. 
		This helps you know if you remove this plugin, what elements will be affected. 
		When scanning files, it only scans PHP files in the active theme's directory.
		The number of directory levels doesn't matter. It'll scan them no matter how many levels the heirarchy has.
		This system uses WordPress's default search engine--not Relevanssi. WordPress looks for the exact characters, while Relevanssi removes the underscore when searching.
		It will not report on shortcodes used in plugins or custom fields, such as those created with ACF. (<a href="https://adambalee.com/search-wordpress-by-custom-fields-without-a-plugin/" target="_blank">future resource to help with includeing custom fields</a>)
		</p>
		<?php

		//what to search content for
		$shortcodes=array(
			"frn_phone",  //sometimes frn_phone is in comments or fields like %%frn_phone%%, so we need the bracket
			"lhn_inpage",
			"frn_social",
			"frn_footer",
			"frn_privacy_url",
			"frn_boxes",
			"idomain",
			"ldomain",
			"frn_sitebase",
			"frn_imagebase",
			"frn_related",
			"frn_related_list"
		);
		frn_shortcodes_posts($shortcodes);

		
		frn_shortcodes_php($shortcodes);
		global $found_php;
		global $found_count;
		echo "\n 
		<div class=\"frn_rounded_box\">
		<h4 style=\"margin:0;\">Found in ".$found_count." PHP Files: </h4>
		<ul class=\"frn_level_1\">
		";
		if($found_count>0) {
			//print_r($found_php);
			foreach( $found_php as $found) {
				if($found_prior==$found[1]) echo ", [".$found[0]."]";
				else echo "<li><b>".$found[1] .":</b> [". $found[0]."]";
				$found_prior = $found[1];
			}
		}
		else echo "<li>No FRN Shortcodes found in PHP files.</li>";
		echo "
		</ul>
		</div>
		<br />
		";
	}

}


function frn_shortcodes_posts($shortcodes) {
	//print_r($shortcodes);
	///Search all content first
	$post_type="any"; //array( 'post', 'page' ),
	//if($post_type!=="any") $post_type=explode(",",$post_type);
	$others=""; $found="";
	foreach($shortcodes as $shortcode) {

		$shortcodes_results = new WP_Query( array( 
			's' 				=> $shortcode,  
			'post_type' 		=> $post_type,
			'posts_per_page' 	=> 0,
			'post_status' 		=> 'publish'
		));

		$total=$shortcodes_results->found_posts;

		if(!$total) $total=0;
		   
		if($total>0) $found.= "<li><b>[" . $shortcode . "]</b>: " . $total . " posts</li>\n";
		else {
			if($others!=="") $others.=",";
			$others.=$shortcode;
		}

		wp_reset_postdata();

	}
	echo "\n 
	<div class=\"frn_rounded_box\">
	<h4 style=\"margin:0;\">Found in Posts: </h4>
	<ul class=\"frn_level_1\">
	";
	if($found!="") {
		echo $found;
	}
	else echo "\n <li>No FRN shortcodes are used in posts.</li>\n";

	if($others!=="") {
		$others = explode(",",$others);
		echo "<li>[<a href='javascript:showhide(\"frn_shortcode_notfound\");'>view shortcodes not found</a>]";
		echo "<ul id='frn_shortcode_notfound' style='display:none;' class=\"frn_level_2\" >";
		foreach($others as $other) {
			echo "<li><b>[" . $other . "]</b>: " . $total . " posts</li>\n";
		}
		echo "</ul></li>";
	}
	echo "
	</ul>
	</div>
	<br />
	";
	 
}



function frn_shortcodes_php($shortcodes = array() , $dir = "" , $count=0 )
{
	global $found_php;
	global $found_count;
	if(!isset($count)) $count=0;
	if(!isset($found_count)) $found_count=0;

	//use theme directory if none provided
	if($dir=="") $dir=get_template_directory();
	//echo $dir."<br />";

    //setting a 2 minute timeout in case there is an issue
    ini_set('max_execution_time', 60); //in seconds

    //echo "<script>var elm = document.getElementById('search');elm.value='$_POST[search]';</script>";

    //get array of files in the directory
    $files = scandir($dir);

    foreach ($files as $file) {
    	
        //make sure current file or directory isn't ones we know we don't want scanned
        if(
        	$file != "." && $file != ".." 
        	&& !strpos($path,"/uploads/") 
        	&& !strpos($path,"/cache/") 
        	&& !strpos($path,"/plugins/") 
        	&& !strpos($path,"/css/") 
        	&& !strpos($path,"/images/")
        	&& !strpos($dir,"/js/")
        	&& $file!=="cache"
        	&& $file!=="css"
        ) {

        	$count++;
        	$path = realpath($dir . DIRECTORY_SEPARATOR . $file);
	        
	        //if the current path is not a directory, cycle through the files in the directory
	        if (!is_dir($path)) {
	        	
				//$filetype = wp_check_filetype($file);
				//print_r($filetype)."<br />";
				//echo "; ext: ".$filetype['ext']."<br />"; 
				//search content only if filetype is PHP
				if( strpos($file,".php")==true ) {

					//echo "<br />Scanning: ".$file;
		            $content = file_get_contents($path);
		            //print_r($shortcodes);
		            //search content for shortcodes
		            $count_prior = $found_count;
		            foreach($shortcodes as $shortcode) {
			            if (strpos($content, $shortcode) !== false) {
			            	//if found, store in variable to print at end
			                $found_php[] = array($shortcode,$file);
			                //found_count is for the number of files affected, not number of shortcodes found
			                if($count_prior==$found_count) $found_count++;
			            }
			            //$results[] = $path;
			        }
	        	}

	        } elseif (
	        	$file != "." 
	        	&& $file != ".." 
	        	&& $file!=="uploads"
	        	&& $file!=="plugins"
	        	&& $file!=="css"
	        	&& $file!=="images"
	        	&& $file!=="js"
        	) {
	            //if directory, then run this function on files within the directory
	            //echo "<h4><b>/".$file."/ is a dir. Rerunning on its files.</b></h2>";
	            frn_shortcodes_php($shortcodes, $path, $count);
	            //$results[] = $path;
	        }
	    }
	    else {
	    	//echo " (skipped)<br />";
	    }
    	
    }

    //return $results;
}

?>