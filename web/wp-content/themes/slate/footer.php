		
		<?php

		//Start defining variables for building the header
		$navtype = get_field('nav_type', 'option');
		$mobilenav = get_field('mobile_navtype', 'option');
		$searchtype = get_field('search_type', 'option');
		$dropdowntype = get_field('dropdown_type', 'option');
		$headeractive = get_field('header_on_off', 'option');
		
		if ($headeractive == false) { ?>
		<!-- header is off -->

			<a id="backtop" class="button" href="#top_anchor" onClick="ga('send', 'event', 'back to top', 'back to top');"><i class="fa fa-angle-up" aria-hidden="true"></i></a>

		<?php } else { ?>
		<!-- header is on -->

			<a id="backtop" class="button" href="#globalheader" onClick="ga('send', 'event', 'back to top', 'back to top');"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
		<?php }

		// Footer Layout
		get_template_part('parts/footer', 'layout'); ?>               
	        
		<!-- Off-Canvas Closers -->
		<?php
		if($mobilenav == 'offcanvas') {
					echo '</div>';// /.off-canvas-content
				echo '</div>';// /.off-canvas-wrapper-inner
			echo '</div>';// /.off-canvas-wrapper
		} 
		?><!-- /Off-Canvas Closers -->
	        

		<?php wp_footer(); ?>

	    <!-- Google Maps Script -->
	    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCuGEkS3XOItflpYiLMK5ClgulNVI1l9TM"></script>

	</body><!-- /body -->
</html> <!-- end page -->