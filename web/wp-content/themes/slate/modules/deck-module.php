<?php 

  // module class/id
  $addclass = get_sub_field('add_moduleclass');
  $addid = get_sub_field('add_moduleid');
  $class = get_sub_field('module_class');
  $id = get_sub_field('module_id');
  
  if ($addclass) {
    $moduleclass = ' '.$class.'';
  }
  if ($addid) {
    $moduleid = ' id="'.$id.'"';
  }

  // Get Card Type
  $optionheader = get_sub_field('optional_header');
  $optionheadertext = get_sub_field('optional_header_text');
  $cardtype = get_sub_field('card_type');
  $deckclass = ' deck--'.$cardtype;
  $columns = get_sub_field('columns');
  $mediumcolumns = get_sub_field('medium_columns');

  if($cardtype == 'highlight') {
    $collapsecolumns = ' large-collapse medium-collapse';
  }
  



?>

<?php
  echo '<div'.$moduleid.' class="module deck'.$deckclass.''.$moduleclass.'">';
?>
  <div class="inner expanded">

    <?php if ($optionheader) { ?>
      <div class="module__header">
        <h1><?php echo $optionheadertext ?></h1>
      </div>
    <?php } ?><!-- end .module__header -->
    
    <!-- Start Deck Module -->
      <?php if( have_rows('deck') ) {
        
          echo '<div class="row expanded'.$collapsecolumns.' large-up-'.$columns.' medium-up-'.$mediumcolumns.' small-up-1" data-equalizer data-equalize-on="medium">';
          
          while( have_rows('deck') ) { the_row();
            
            // Class Fields
            $cardprefix = "card--";
            $buttonprefix = "button--";
            $cardclass = $cardprefix . $cardtype;
            $buttonclass = $buttonprefix . $cardtype;

            // Image Fields
            $businessimage = get_sub_field('business_image');
            $iconimage = get_sub_field('icon_image');
            $testimonialimage = get_sub_field('testimonial_image');
            $highlightimage = get_sub_field('highlight_image');

            // Basic Card Clone
            $card = get_sub_field(''.$cardtype.'_card');
            $cardheader = $card['card_header'];
            $cardsubhead = $card['card_secondary'];
            $cardsubtext = $card['card_secondary_text'];
            if($cardsubhead === 'div') {
              $cardsubtext = apply_filters('the_content', $card['card_secondary_text']);
            }
            
            // Link Wrap Fields
            $addlink = $card['add_link'];
            $linktarget = $card['link_target'];
            $linkselect = $card['link_select'];
            $modaltarget = $card['modal_target'];
            
            if ($addlink) {
              $cardtagopener = 'a';
              $cardtagcloser = '</a>';
              $cardlink = ' href="'.$linktarget.'"';
              $cardlinkclass = ' card__holder--link';
            } else {
              $cardtagopener = 'div';
              $cardtagcloser = '</div>';
            }

            // Define Card Holder
            if($linkselect === 'modal') {
              $cardholder = '<a class="card__holder'.$cardlinkclass.' card__holder--'.$cardtype.'" data-open="'.$modaltarget.'" onClick="ga(\'send\', \'event\', \'modal button\', \'opens the modal\');" data-equalizer-watch>';
            } else {
            $cardholder = '<'.$cardtagopener.''.$cardlink.' class="card__holder'.$cardlinkclass.' card__holder--'.$cardtype.'" data-equalizer-watch>';
            }// /if modal

            // Button Fields
            $addbutton = $card['add_button'];
            $button = $card['button'];
            $buttonselect = $button['button_target_select'];
            $buttontarget = $button['button_target'];
            $buttontext = $button['button_text'];
            $modaltarget = $button['modal_target'];

            ?>


            <div class="column card <?php echo $cardclass ?>">
            
              <?php
              // START CARDS

                // BASIC
                if ($cardtype == "basic") {
                  
				          echo $cardholder;// <div or <a class="card__holder"
                    // Header
                    echo'<h2 class="card__header card__header--basic">'.$cardheader.'</h2>';
                    // Subhead/Paragraph
                    if ($cardsubhead){
                      echo '<'.$cardsubhead.' class="card__secondary card__secondary--basic">'
                           .$cardsubtext.
                          '</'.$cardsubhead.'>';
                    }

                    // Button
                    if ($addbutton){

                      if ($addlink) { // avoid nested <a> by switching to <span>
                        echo '<span class="button '.$buttonclass.'">'.$buttontext.'</span>';
                      } else {

                        if($buttonselect === 'modal') {
                          echo '<a class="button button--modal '.$buttonclass.'" data-open="'.$modaltarget.'" onClick="ga(\'send\', \'event\', \'modal button\', \'opens the modal\');">'.$buttontext.'</a>';
                        } else {
                          echo '<a href="'.$buttontarget.'" class="button '.$buttonclass.'">'.$buttontext.'</a>';  
                        }// /if modal
                        
                      }// if addlink
                    }// /Button

                  echo $cardtagcloser;//end card__holder
                }// /BASIC
              


                // BUSINESS
                if ($cardtype == "business") {
                  echo $cardholder;// <div or <a class="card__holder"
                    
                    // Image
                    if ($businessimage){
                      echo '<div class="card__image card__image--business">
                            <img src="'.$businessimage['url'].'" />
                          </div>';	
                    }

                    // Header
				            echo '<div class="card__info--business">';
                      echo'<h2 class="card__header card__header--business">'.$cardheader.'</h2>';
                  
                      // Subhead/Paragraph
                      if ($cardsubhead){
                        echo '<'.$cardsubhead.' class="card__secondary card__secondary--business">'
                             .$cardsubtext.
                            '</'.$cardsubhead.'>';
                      }

                      // Button
                    if ($addbutton){

                      if ($addlink) { // avoid nested <a> by switching to <span>
                        echo '<span class="button '.$buttonclass.'">'.$buttontext.'</span>';
                      } else {

                        if($buttonselect === 'modal') {
                          echo '<a class="button button--modal '.$buttonclass.'" data-open="'.$modaltarget.'" onClick="ga(\'send\', \'event\', \'modal button\', \'opens the modal\');">'.$buttontext.'</a>';
                        } else {
                          echo '<a href="'.$buttontarget.'" class="button '.$buttonclass.'">'.$buttontext.'</a>';  
                        }// /if modal
                        
                      }// if addlink
                    }// /Button

                    echo '</div>';//close card__info-business
        					echo $cardtagcloser;//end card__holder
                }// /BUSINESS
              


                // ICON
                if ($cardtype == "icon") {
                  echo $cardholder;// <div or <a class="card__holder"
                  
                    // Image
                    if ($iconimage){
                      echo '<div class="card__image card__image--icon">
                                                <img src="'.$iconimage['url'].'" />
                                              </div>';
                    }

                    // Header
                    echo'<h2 class="card__header card__header--icon">'.$cardheader.'</h2>';
                    
                    // Subhead/Paragraph
                    if ($cardsubhead){
                      echo '<'.$cardsubhead.' class="card__secondary card__secondary--icon">'
                           .$cardsubtext.
                          '</'.$cardsubhead.'>';
                    }

                      // Button
                    if ($addbutton){

                      if ($addlink) { // avoid nested <a> by switching to <span>
                        echo '<span class="button '.$buttonclass.'">'.$buttontext.'</span>';
                      } else {

                        if($buttonselect === 'modal') {
                          echo '<a class="button button--modal '.$buttonclass.'" data-open="'.$modaltarget.'" onClick="ga(\'send\', \'event\', \'modal button\', \'opens the modal\');">'.$buttontext.'</a>';
                        } else {
                          echo '<a href="'.$buttontarget.'" class="button '.$buttonclass.'">'.$buttontext.'</a>';  
                        }// /if modal
                        
                      }// if addlink
                    }// /Button
                      
                  echo $cardtagcloser;//end card__holder
                }// /ICON
              


                // TESTIMONIAL
                if ($cardtype == "testimonial") {
                  // Image
                  if ($testimonialimage){
                    echo '<div class="card__image card__image--testimonial" style="background: url('.$testimonialimage.') center center/cover no-repeat;" data-equalizer-watch></div>';
                  }
                  // Header
                  echo'<h2 class="card__header card__header--testimonial">'.$cardheader.'</h2>';
                  // Subhead/Paragraph
                  if ($cardsubhead){
                    echo '<'.$cardsubhead.' class="card__secondary card__secondary--testimonial">'
                         .$cardsubtext.
                        '</'.$cardsubhead.'>';
                  }
                  // Button
                    if ($addbutton){

                      if ($addlink) { // avoid nested <a> by switching to <span>
                        echo '<span class="button '.$buttonclass.'">'.$buttontext.'</span>';
                      } else {

                        if($buttonselect === 'modal') {
                          echo '<a class="button button--modal '.$buttonclass.'" data-open="'.$modaltarget.'" onClick="ga(\'send\', \'event\', \'modal button\', \'opens the modal\');">'.$buttontext.'</a>';
                        } else {
                          echo '<a href="'.$buttontarget.'" class="button '.$buttonclass.'">'.$buttontext.'</a>';  
                        }// /if modal
                        
                      }// if addlink
                    }// /Button
                }// /TESTIMONIAL
              


                // HIGHLIGHT
                if ($cardtype == "highlight") {
                  if ($highlightimage) {
                    $highlightimagestring = ' style="background-image: url('.$highlightimage.')"';
                  }

                  echo $cardholder;// <div or <a class="card__holder"
                  
                    echo '<div class="highlight__box"'.$highlightimagestring.' data-equalizer-watch>';

                      // Header
                      if ($cardheader) {
                      echo'<h2 class="card__header card__header--highlight">'.$cardheader.'</h2>';  
                      }
                      
                      // Subhead/Paragraph
                      if ($cardsubhead){
                        echo '<'.$cardsubhead.' class="card__secondary card__secondary--highlight">'
                             .$cardsubtext.
                            '</'.$cardsubhead.'>';
                      }
                   
                    echo '</div>';// /.highlight__box
                    
                  echo $cardtagcloser;//end card__holder
                  
                }// /HIGHLIGHT
  


                // WILDCARD
                if ($cardtype == "wildcard") {
                  get_template_part('modules/wildcard', 'card');
                }// /WILDCARD
                ?>
              


            </div><!-- end .column .card-->

        <?php } // end while have_rows ?>
      </div><!-- end .row -->
    <?php }// end if have_rows ?>

  </div><!-- end .inner -->
</div><!-- end .deck -->