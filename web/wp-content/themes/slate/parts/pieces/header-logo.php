

<!-- Logo -->

<a href="<?php echo home_url('/'); ?>">

	<!-- this grabs the Logo from the themes page, if they do not upload one it defaults to the one below -->
	<?php if( get_field('site_logo', 'option') ) { ?>
	  <img src="<?php the_field('site_logo', 'option'); ?>" alt="<?php bloginfo('name'); ?>" class="img-responsive" id="logo" />
	<?php } else {?>
	  <img src="<?php echo get_template_directory_uri(); ?>/assets/images/generic_logo.svg" alt="<?php bloginfo('name'); ?>" class="img-responsive" id="logo" />	
	<?php } ?>

</a>