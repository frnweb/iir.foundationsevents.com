<?php 

// Footer Creds

$credlargewidth = get_field('cred_large_width', 'option');
$credmediumwidth = get_field('cred_medium_width', 'option');

if( have_rows('footer_accreditations', 'option') ) {

	echo '<div id="footer-creds" class="row cred__grid large-up-'.$credlargewidth.' medium-up-'.$credmediumwidth.' small-up-'.$credmediumwidth.'">';

		while( have_rows('footer_accreditations', 'option') ) { the_row(); 

			$credimage = get_sub_field('cred_image');

			echo '<div class="column cred large-centered medium-centered">';
				echo '<img class="cred__image" src="'.$credimage['url'].'" />';
			echo '</div>';// /cred
		}// /while have rows

	echo '</div>';// /.row.expanded.footer__creds

}// /if have rows

?>