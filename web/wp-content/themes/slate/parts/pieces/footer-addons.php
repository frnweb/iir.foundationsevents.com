<?php 

// Footer Addons

$footeraddons = get_field('footer_addons', 'option');

if($footeraddons) {
	echo '<div id="footer-addons" class="row text-center">';
		echo $footeraddons;
	echo '</div>';
}// if $footeraddons

?>