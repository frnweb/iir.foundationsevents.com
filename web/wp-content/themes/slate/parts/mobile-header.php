<div id="mobilephone" class="show-for-small-only">Confidential and Private  <strong><?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Sticky Header bar on Mobile"]'); ?></strong></div><!-- end mobile phone -->
			
<?php 

//Start defining variables for building the header
$navtype = get_field('nav_type', 'option');
$mobilenav = get_field('mobile_navtype', 'option');
$searchtype = get_field('search_type', 'option');
$dropdowntype = get_field('dropdown_type', 'option');

// Config the data-toggle for the hamburger button
if($mobilenav == 'offcanvas') {
	$navtoggle = 'off-canvas';
}
if($mobilenav == 'popover') {
	$navtoggle = 'popover';
}


echo '<div class="title-bar show-for-small-only" data-responsive-toggle="top-bar-menu">';
	echo get_template_part( 'parts/pieces/header', 'logo' );
?>
<!--start the mobile menu button -->
<div id="mobilemenu" class="show-for-small-only small-3 columns">
	<ul class="menu">
		<li>
			<?php
			// Hamburger button with Data Toggle
			echo '<button type="button" class="hamburger nav-button hamburger--squeeze" data-toggle="'.$navtoggle.'" onClick="ga("send", "event", "hamburger Menu", "opens mobile menu button");">';
			?>
				<span class="hamburger-box">
					<span class="hamburger-inner"></span>
				</span>
			</button><!-- /.hamburger.nav-button.hamburger -->
		</li>
		
		<li>
			<?php
			echo '<a data-toggle="'.$navtoggle.'">';
				echo _e( 'Menu', 'jointswp' );
			echo '</a>';
			?>
		</li>
		
	</ul><!-- end ul.menu -->
</div><!-- end #mobilemenu -->

<?php
	echo '</div>';
?>