	 <article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/WebPage">
  	 <section itemprop="articleBody">
<?php //this conditional statement is where we define some layout options for the different template pages
			if ( is_page_template( 'template-home.php' ) ) {
				//home page template	
				//echo '<span class="success label">this is the Home Page template</span>';
				echo '<div id="content" class="inner expanded page--home">';
			} 
			elseif ( is_page_template( 'template-full-width.php' ) ) {
			  // landing page
				//echo '<span class="success label">this is the Landing page template</span>';
				echo get_template_part('parts/loop', 'pagetitle');//grabs the page title
				echo '<div id="content" class="inner expanded page--landing">';
			
			} 
		 
		 elseif ( is_page_template( 'template-staff-members.php' ) ) {
			  // staff members page
				echo get_template_part('parts/loop', 'pagetitle');//grabs the page title
				echo '<div id="content" class="inner expanded page--staff">';
			
			} 
		 
		 elseif ( is_page_template( 'template-formatting.php' ) ) {
			  // formatting page
				//echo '<span class="success label">this is the Landing page template</span>';
				echo get_template_part('parts/loop', 'pagetitle');//grabs the page title
				echo '<div id="content" class="inner expanded page--formatting">';
				echo get_template_part('parts/loop', 'formatting');//grabs the sample formatting page stuff
			} 
		
			else {
			  //the page template is not defined
				//echo '<span class="warning label">They Have not picked a Template Page, so this is the default</span>';
				echo get_template_part('parts/loop', 'pagetitle');//grabs the page title
				echo '<div id="content" class="inner page--default with-sidebar">';
				echo '<div id="inner-content" class="row expanded large-collapse medium-collapse">';
				
          // start sidebar and main layouts
          $sidebarlayout = get_field('sidebar_layout', 'option');
          $largesidebarwidth = get_field('large_sidebar_width', 'option');
          $mediumsidebarwidth = get_field('medium_sidebar_width', 'option');

          $largewidth = (12 - $largesidebarwidth);
          $mediumwidth = (12 - $mediumsidebarwidth);


          // #main
          if($sidebarlayout == 'left') {
            echo '<main id="main" class="large-'.$largewidth.' medium-'.$mediumwidth.' large-push-'.$largesidebarwidth.' medium-push-'.$mediumsidebarwidth.' columns" role="main">';
          }

          if($sidebarlayout == 'right') {
            echo '<main id="main" class="large-'.$largewidth.' medium-'.$mediumwidth.' columns" role="main">';
          }
  				echo the_content();
  				echo '</main>';//end #main

          // sidebar
          if($sidebarlayout == 'left') {
            echo '<div id="sidebar1" class="sidebar large-'.$largesidebarwidth.' medium-'.$mediumsidebarwidth.' large-pull-'.$largewidth.' medium-pull-'.$mediumwidth.' columns" role="complementary">';
          }

          if($sidebarlayout == 'right') {
            echo '<div id="sidebar1" class="sidebar large-'.$largesidebarwidth.' medium-'.$mediumsidebarwidth.' columns" role="complementary">';
          }
    	   	echo get_sidebar();
          echo '</div>';//end the sidebar

        echo '</div>';//end the #inner content 
			}
		?>
   

   
   	<div class="flexible-content">
    
    <?php

    // are there any rows in our flexible content field?
    if( have_rows('flexible-content') ) {

      // loop through all the rows of flexible content
      while ( have_rows('flexible-content') ) { the_row();

        // DECK
        if( get_row_layout() == 'deck_clone' )
            get_template_part('modules/deck', 'module');

        // BILLBOARD
        if( get_row_layout() == 'billboard_clone' )
            get_template_part('modules/billboard', 'module');

        // SPOTLIGHT
        if( get_row_layout() == 'spotlight_clone' )
            get_template_part('modules/spotlight', 'module');

        // DUO
        if( get_row_layout() == 'duo_clone' )
            get_template_part('modules/duo', 'module');

        // GALLERY
        if( get_row_layout() == 'gallery_clone' )
            get_template_part('modules/gallery', 'module');

        // CONTENT EDITOR
        if( get_row_layout() == 'wysiwyg_content_clone' )
  		  //echo '<div class="entry-content inner">';
            get_template_part('modules/wysiwyg', 'module');
  		  //echo '</div>';

        // CAROUSEL
        if( get_row_layout() == 'carousel_clone' )
            get_template_part('modules/carousel', 'module');

        // STAFF
        if( get_row_layout() == 'staff_clone' )
            get_template_part('modules/staff', 'module');

        // CONTACT
        if( get_row_layout() == 'contact_clone' )
            get_template_part('modules/contact', 'module');

        // INSURANCE
        if( get_row_layout() == 'insurance_clone' )
            get_template_part('modules/insurance', 'module');

        // CUSTOM PHP
        if( get_row_layout() == 'custom_php' )
            get_template_part('modules/custom', 'module');

        // THE CONTENT
        if( get_row_layout() == 'the_content' )
            get_template_part('modules/content', 'module');

        // MODAL
        if( get_row_layout() == 'modal_clone' )
            get_template_part('modules/modal', 'module');

        // WRAPPER OPEN
        if( get_row_layout() == 'wrapper_open' )
            get_template_part('modules/wrapper_open', 'module');
        
        // WRAPPER CLOSE
        if( get_row_layout() == 'wrapper_close' )
            get_template_part('modules/wrapper_close', 'module');

      } // close the loop of flexible content
    } // close flexible content conditional
  ?>
  <!-- End FLEXIBLE CONTENT MODULES FIELD -->

</div><!-- end flexible content -->
		
			<?php
			 if ( is_page_template( 'template-staff-members.php' ) ) { 
			 		echo get_template_part('parts/staff', 'list');//grabs the page title	
			 }	
		 	?>
	
</div><!-- this closes the divs for all of the #content opening tags in the conditional statement at the beginning -->


</section> <!-- end article section -->
</article> <!-- end article -->