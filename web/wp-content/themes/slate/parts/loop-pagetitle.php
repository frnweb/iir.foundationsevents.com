<?php 
/* This is where we are defining how the page titles are appearing throughout different parts of the site*/
?>

<header class="article-header">

  
  <?php 
				if ( is_front_page() && is_home() ) {
				  // Default homepage
					} 
				elseif ( is_front_page() ) {
				  // static homepage
					} 
				elseif ( is_home() ) {
				  // blog page
					echo '<h1 class="page-title">Resources</h1>';
					} 
		
				elseif ( is_search() ) {
					//show the search string 
					echo '<h1 class="page-title">';
					_e( 'Search Results for: ', 'jointswp' ); 
					echo esc_attr(get_search_query());
					echo '</h1>';
					}
				
				elseif ( is_archive() ) {
					echo '<h1 class="page-title">';
					echo the_archive_title();
					echo '</h1>';
					//echo the_archive_description('<div class="taxonomy-description">', '</div>');
					}
	
				  else {
				  echo '<h1 class="page-title">'.get_the_title().'</h1>';
				}
	?>
  
</header><!-- end .article header -->