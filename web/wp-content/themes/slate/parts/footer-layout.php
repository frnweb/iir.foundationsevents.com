<?php
// Footer Layout

$navtype = get_field('nav_type', 'option');
$footerlogo = get_field('footer_logo', 'option');
$sitetitle = get_bloginfo('name');


// Footer Location
get_template_part('parts/footer', 'location'); ?>


<footer class="footer" role="contentinfo">            
    <div class="inner section">
        
        <div class="row footer-branding expanded large-collapse medium-collapse">
        	
        	<!-- Piece-Logo -->
        	<div class="columns large-8 medium-7">
            	<?php get_template_part('parts/pieces/footer', 'logo'); ?>
        	</div><!-- /.columns large-6 -->
        	
			<!-- Piece-Socials -->
           	<div class="columns large-4 medium-5">
            	<?php get_template_part( 'parts/pieces/footer', 'socials' ); ?>
            </div><!-- /.columns large-4 -->
        
        </div> <!-- /.row -->
    
    	<!-- Piece-Nav -->
    	<nav role="navigation" class="show-for-medium clearfix">
			<?php joints_footer_links(); ?>
		</nav><!-- /navigation -->

    </div><!-- /.inner section -->
        	
<?php 

// Accreditations
get_template_part('parts/pieces/footer', 'accreditations');

// Addons
get_template_part('parts/pieces/footer', 'addons');

echo do_shortcode('[frn_footer]'); ?>

</footer> <!-- end .footer -->