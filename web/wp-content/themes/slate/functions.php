<?php



// Theme support options
require_once(get_template_directory().'/assets/functions/theme-support.php'); 

// WP Head and other cleanup functions
require_once(get_template_directory().'/assets/functions/cleanup.php'); 

// Register scripts and stylesheets
require_once(get_template_directory().'/assets/functions/enqueue-scripts.php'); 

// Register custom menus and menu walkers
require_once(get_template_directory().'/assets/functions/menu.php'); 

// Custom Megamenu Function for Foundation Recovery Network
require_once(get_template_directory().'/assets/functions/megamenu.php');

// Custom Shortcodes for Foundation Recovery Network
require_once(get_template_directory().'/assets/functions/shortcodes.php'); 

// Register sidebars/widget areas
require_once(get_template_directory().'/assets/functions/sidebar.php'); 

// Makes WordPress comments suck less
require_once(get_template_directory().'/assets/functions/comments.php'); 

// Replace 'older/newer' post links with numbered navigation
require_once(get_template_directory().'/assets/functions/page-navi.php'); 

// Adds support for multiple languages
require_once(get_template_directory().'/assets/translation/translation.php'); 

// Remove 4.2 Emoji Support
require_once(get_template_directory().'/assets/functions/disable-emoji.php'); 

// Adds site styles to the WordPress editor
//require_once(get_template_directory().'/assets/functions/editor-styles.php'); 

// Related post function - no need to rely on plugins
// require_once(get_template_directory().'/assets/functions/related-posts.php'); 

// Use this as a template for custom post types
require_once(get_template_directory().'/assets/functions/custom-post-type.php');

// Function for hiding/showing fields on ACF Flexible Content Fields
require_once(get_template_directory().'/assets/functions/acf-layout.php'); 

add_action('acf/input/admin_head', 'acf_admin_head_layout');

////this helps pull in the ACF JSON fields into the Child Themes 
add_filter('acf/settings/save_json', function() {
	return get_stylesheet_directory() . '/acf-json';
});

add_filter('acf/settings/load_json', function($paths) {
	$paths = array(get_template_directory() . '/acf-json');

	if(is_child_theme())
	{
		 $paths = array(
            get_stylesheet_directory() . '/acf-json',
            get_template_directory() . '/acf-json'
        );
	}

	return $paths;
});

function my_acf_init() {
	
	acf_update_setting('google_api_key', 'AIzaSyCuGEkS3XOItflpYiLMK5ClgulNVI1l9TM');
}

add_action('acf/init', 'my_acf_init');

//////////
// RSS Feed Template Redirect to Ours - Removes Author from RSS Feeds
// Code by Dax - Added by Ben W. on 3.29.17
//////////
remove_all_actions( 'do_feed_rss2' );  
function frn_feed_template_redir() {  
    load_template( TEMPLATEPATH . '/feed-rss2.php');  
}  
add_action('do_feed_rss2', 'frn_feed_template_redir', 10, 1);

// TGM Plugin Activation
require_once('tgm-plugin-activation/class-tgm-plugin-activation.php');
require_once('tgm-plugin-activation/required_plugins.php');


// Theme Update Checker
require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://bitbucket.org/frnweb/slate',
	__FILE__,
	'slate'
);
//Optional: If you're using a private repository, create an OAuth consumer
//and set the authentication credentials like this:
$myUpdateChecker->setAuthentication(array(
	'consumer_key' => 'NF7w7stHHYVA9mFkGq',
	'consumer_secret' => '56fD2w9RnKquZh2b9En9kWh3vzEELPyw',
));

//Optional: Set the branch that contains the stable release.
$myUpdateChecker->setBranch('master');

