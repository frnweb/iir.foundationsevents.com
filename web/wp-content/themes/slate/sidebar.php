<div id="sidebar-container" data-sticky-container class="show-for-medium">
<div id="sidebarcontent" class="sticky" data-sticky data-anchor="inner-content">
	<div class="sidebarinside clearfix">
  
  
  	
    
	<?php
	$pagetitle = get_the_title($post->post_parent);

	if ($post->post_parent)	{
		$ancestors=get_post_ancestors($post->ID);
		$root=count($ancestors)-1;
		$grandparent_id = $ancestors[$root];
		$toplevel = get_the_title($grandparent_id);
		$parent_id = $post->ID;

		} else {

		}
	?>

	<?php if( ($post->post_parent) && (is_tree($grandparent_id) || is_tree($parent_id)) )  : //this grabs the sub menu for the about page	?>

		<h2 class="sidebarpagetitle">
		  	<?php
		  	if ( 0 == $post->post_parent ) {
					the_title();
				} else {
					$parents = get_post_ancestors( $post->ID );
					echo apply_filters( "the_title", get_the_title( end ( $parents ) ) );
				}
			?>		
		</h2>


		<?php
			wp_nav_menu( array('menu' => ''.$toplevel.' sub-menu') );
	
     ?>
	 
             
           
       <?php elseif ( is_active_sidebar( 'sidebar1' ) ) : ?>

		<?php dynamic_sidebar( 'sidebar1' ); ?>

	<?php else : ?>

	<!-- This content shows up if there are no widgets defined in the backend. -->
						
	

	<?php endif; ?>
    </div><!-- end sidebarinside -->
</div><!-- end #sidebarcontent -->
</div><!-- end sticky container -->
