<?php


function slate_scripts() {
  global $wp_styles; // Call global $wp_styles variable to add conditional wrapper around ie stylesheet the WordPress way
    
    // Load What-Input files in footer
    wp_enqueue_script( 'jQuery', get_template_directory_uri() . '/vendor/jquery/dist/jquery.min.js', array('jquery'), '2.2.3', true );
	
	// Load What-Input files in footer
    wp_enqueue_script( 'what-input', get_template_directory_uri() . '/vendor/what-input/what-input.min.js', array(), '', true );
    
    // Adding Foundation scripts file in the footer
    wp_enqueue_script( 'foundation-js', get_template_directory_uri() . '/vendor/foundation-sites/dist/foundation.min.js', array( 'jquery' ), '6.0', true );
	    
    // Compiled Slate scipts
    wp_enqueue_script( 'slate-js', get_template_directory_uri() . '/assets/js/slate.min.js', array(), '', true );

    // WP_localize_script for template directory
    $directory = array( 'stylesheet_directory_uri' => get_stylesheet_directory_uri() );
wp_localize_script( 'slate-js', 'themepath', $directory );

    // Adding bx-sliders for slate
    //wp_enqueue_script( 'custom-bxsliders', get_template_directory_uri() . '/assets/js/custom-bxsliders.js', array( 'jquery' ), '', true );

    // CSS
    // Register Motion-UI
    wp_enqueue_style( 'motion-ui-css', get_template_directory_uri() . '/vendor/motion-ui/dist/motion-ui.min.css', array(), '', 'all' );
    
    // Select which grid system you want to use (Foundation Grid by default)
    wp_enqueue_style( 'foundation-css', get_template_directory_uri() . '/vendor/foundation-sites/dist/foundation.min.css', array(), '', 'all' );

    // Compiled Slate styles
    wp_enqueue_style( 'slate-css', get_template_directory_uri() . '/assets/css/slate.min.css', array(), '', 'all' );

    // Adding FontAwesome
    wp_enqueue_script( 'theme_fontawesome', '//use.fontawesome.com/e5d0f64faa.js');


    // Comment reply script for threaded comments
    if ( is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
      wp_enqueue_script( 'comment-reply' );
    }
}
add_action('wp_enqueue_scripts', 'slate_scripts', 999);

// Async load
function add_async_attribute($tag, $handle) {
   // add script handles to the array below
   $scripts_to_async = array('', '');
   
   foreach($scripts_to_async as $async_script) {
      if ($async_script === $handle) {
         return str_replace(' src', ' async="async" src', $tag);
      }
   }
   return $tag;
}



add_filter('script_loader_tag', 'add_async_attribute', 10, 2);

function add_defer_attribute($tag, $handle) {
   // add script handles to the array below
   $scripts_to_defer = array('bx-slider', 'custom-js', 'viewport-checker');
   
   foreach($scripts_to_defer as $defer_script) {
      if ($defer_script === $handle) {
         return str_replace(' src', ' defer="defer" src', $tag);
      }
   }
   return $tag;
}
add_filter('script_loader_tag', 'add_defer_attribute', 10, 2);
