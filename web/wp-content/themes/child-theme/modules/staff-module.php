<?php
/*
Module: Staff
*/
?>

<?php
$optionheader = get_sub_field('optional_header');
$optionheadertext = get_sub_field('optional_header_text');

// Deck Columns
$columns = get_sub_field('columns');
if ($columns == '4') {
    $mediumcolumns = 2;
  } elseif ($columns == '2') {
    $mediumcolumns = 2;
  } elseif ($columns == '1') {
    $mediumcolumns = 1;
  } else {
    $mediumcolumns = 3;
  }  

// module id
$staffid = get_sub_field('staff_id');
$addid = $staffid['add_moduleid'];
$id = $staffid['module_id'];

$stafftype = get_sub_field('staff_type');
$staffcarousel = get_sub_field('staff_carousel');
$stafflink = get_sub_field('staff_link');

if ($addid && $id) {
		$moduleid = ' id="'.$id.'"';
	} else {
		$moduleid = ' id="carousel--staff"';
	}

if ($stafftype == 'deck') {
	$deck = '<div class="row expanded staff__deck large-up-'.$columns.' medium-up-'.$mediumcolumns.' small-up-1 large-centered" data-equalizer data-equalize-on="medium">';
}

if ($stafftype == 'carousel') {
	$carousel = '<div'.$moduleid.'class="staff__carousel">';
}

// Carousel Or Deck
echo '<div id="speakers" class="module staff staff--'.$stafftype.'">';
	echo '<div class="inner expanded">';

		// Optional Header
		if ($optionheader) { ?>
	      <div class="module__header">
	        <h1><?php echo $optionheadertext ?></h1>
	      </div>
		<?php } // /.module__header

		
		// Posts
		$posts = get_sub_field('staff_members');

		if ($posts) { 

			echo '<div class="staff__container">';

				echo $deck;// <div class="row expanded staff__deck....
				echo $carousel;// <div id="carousel--staff" class="staff__carousel">

					foreach( $posts as $post) {
						setup_postdata($post);				

						echo '<div class="column column-block staff__card large-centered">';//may need a conditional statement for carousel to remove the column stuff

							// Check if the Staff Card will link to their individual staff pages
							if($stafflink) {
								echo '<a href="#speaker-'.get_the_ID().'" class="staff__holder open" data-equalizer-watch>';
							} else {
								echo '<div class="staff__holder" data-equalizer-watch>';
							}

						    	if( have_rows('staff_format') ) {
							      	// loop through all the rows of flexible content
							      	while ( have_rows('staff_format') ) { the_row() ;

							      		// Staff Fields
										$firstname = get_field('first_name', $post->ID);
										$lastname = get_field('last_name', $post->ID);
										$title = get_field('title', $post->ID);
										$certifications = get_field('certifications', $post->ID);
										$photo = get_field('staff_photo', $post->ID);
										$biosummary = get_field('bio_summary', $post->ID);

										// Wrapper Fields
										$wrapper = get_sub_field('wrapper');
										$wrapperaddclass = $wrapper['add_moduleclass'];
										$wrapperaddid = $wrapper['add_moduleid'];
										$wrapperclass = $wrapper['module_class'];
										$wrapperid = $wrapper['module_id'];

												// NAME
												if( get_row_layout() == 'name' ) {
													
													if ($firstname || $lastname) {
														echo '<h2>';
															if ($firstname) {
															echo $firstname;
															}
															if ($lastname) {
															echo '&nbsp;'.$lastname.'';
															}
															if ($certifications) {
															echo '<span class="certifications">'.$certifications.'</span>';
															}
														echo '</h2>';
													}// /if name
												}// /NAME
																		
												// TITLE
												elseif( get_row_layout() == 'title' ) {
													if ($title) {
														echo '<h3>'.$title.'</h3>';
													}
												}// /TITLE

												// PHOTO
												elseif( get_row_layout() == 'photo' ) {
													if ($photo) {
														echo '<div class="staff__photo">';
															echo '<img src="'.$photo['url'].'" class="staff__img"/>';
														echo '</div>';
													}// /if photo
												}// /PHOTO

												// BIO SUMMARY
												elseif( get_row_layout() == 'bio_summary' ) {
													if ($biosummary) {
													echo '<p>'.$biosummary.'</p>';
													}
												}// /BIO SUMMARY

												// WRAPPER OPEN
												elseif( get_row_layout() == 'wrapper_open') {
													// Prepare class's and id's
														if($wrapperaddclass) {
															$classstring = ' class="'.$wrapperclass.'"';
														}
														if($wrapperaddid) {
															$idstring = ' id="'.$wrapperid.'"';
														}

													// echo a div with the class/id if they've added them
													echo '<div' . $idstring . $classstring . '>';
												}// /WRAPPER OPEN

												// WRAPPER CLOSE
												elseif( get_row_layout() == 'wrapper_close') {
													echo '</div>';
												}// /WRAPPER CLOSE

										}// /while have_rows
									}// /if have-rows

							if($stafflink) {
								echo '</a>';// /permalink
							} else {
								echo '</div>';
							}// /if $stafflink
						
						echo '</div>';// /.column.staff__card
			
					
		      		} // /foreach $posts
	      		echo '</div>';// /$deck or $carousel
	      	echo '</div>';// /.staff__container
	    } // /if $posts

	    wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly

	echo '</div>';// /.inner.expanded
echo '</div>';// .module.staff

?>

<?php

// The Query
$speakerbox = new WP_Query( array( 'post_type' => 'staff' ) );
echo '<div class="speaker__information">';
if ( $speakerbox->have_posts() ) {
	// The Loop
	while ( $speakerbox->have_posts() ) {
		$speakerbox->the_post();
		echo '<a name="speaker-'.get_the_ID().'" style="visibility: hidden;"></a>';///don't remove this, it is for the JS function 
		echo '<div class="speaker__box" id="speaker-'.get_the_ID().'">';///speaker box div - don't change the name of this without updating the JS 
		echo '<div class="row expanded" data-equalizer >';//start row 
		echo '<div class="large-4 medium-3 columns">';
		echo '<div class="photo--holder" data-equalizer-watch>';
		if (get_field('staff_photo')) {
			echo '<div class="staff__photo" style="background-image: url('.get_field('staff_photo')['url'].');">';
					//echo '<img src="'.get_field('staff_photo')['url'].'" class="staff__img"/>';
					echo '</div>';
			}///if photo
                echo '</div>';///end test div      
		echo '</div>';///end left column with photo 
		
		echo '<div class="large-8 medium-9 columns">';
                echo '<div class="staff--card" data-equalizer-watch>';
		
                ////spits out name and ceritifcation 
		echo '<h2>';
		if (get_field('first_name')) {
		echo get_field('first_name');///grabs last name if it exists
		}
		if (get_field('last_name')) {
		echo '&nbsp;'.get_field('last_name').'';///grabs last name if it exists
		}
		if (get_field('certifications')) {
		echo '<span class="certifications">'.get_field('certifications').'</span>';///grabs certicifation if it exists
		}
		echo '</h2>';///end of H@ that wraps name
		
		if (get_field('title')) {
		echo '<h3>'.get_field('title').'</h3>';///grabs title if it exists 
		}
		
		echo '<p>'.get_the_content().'</p>';///grabs the bio if it is in the wysiwyg 
		
		echo '</div>';///end test div      
		echo '</div>';///end col 
		echo '</div>';///end .speaker__box
		echo '</div>';///end  row 

	}

	wp_reset_postdata();
}
 echo '</div>' ?>
<p class="sub-text">For more information about our speakers visit <a href="https://foundationsevents.com/innovations-in-recovery/speakers/">https://foundationsevents.com/innovations-in-recovery/speakers/</a></p>