<div id="" class="module wysiwyg dark-angle-background hide-for-medium">
    <div class="inner">
        <div class="entry-content">
            <h1>Agenda</h1>
            <ul class="tabs " data-tabs id="agenda-tabs">
                <li class="tabs-title  " role="presentation"><a href="#sundaypanel" role="tab" aria-controls="sundaypanel" aria-selected="false" id="sundaypanel-label"><strong>SUNDAY</strong><br>APR 8</a></li>
                <li class="tabs-title  is-active" role="presentation"><a href="#mondaypanel" role="tab" aria-controls="mondaypanel" aria-selected="true" id="mondaypanel-label"><strong>DAY 1-3</strong><br>APR 9-11</a></li>
                <li class="tabs-title  " role="presentation"><a href="#tuesdaypanel" role="tab" aria-controls="tuesdaypanel" aria-selected="false" id="tuesdaypanel-label"><strong>FINAL DAY</strong><br>APR 12</a></li>
            </ul><!-- agenda tabs ul -->
            <div class="tabs-content" data-tabs-content="agenda-tabs">
                <div class="tabs-panel " id="sundaypanel" role="tabpanel" aria-hidden="true" aria-labelledby="sundaypanel-label">
                            <h1><strong>SUNDAY</strong></h1><h1>APR 8</h1>
                            <div class="container panel">
                            <div class="container">
                                <p><strong>Golf Tournament</strong><br>8:00 AM - 12:00 PM</p>
                                <p><strong>Exhibitor Setup</strong><br>1:00 PM - 6:00 PM</p>
                                <p><strong>Early Registration</strong><br>1:00 PM - 7:30 PM</p>
                                <p><strong>Welcome Reception</strong><br>6:30 PM - 8:30 PM</p>
                            </div><!--container half -->
                            </div><!-- container panel -->
                    </div><!--tabs panel -->
                <div class="tabs-panel " id="mondaypanel" role="tabpanel" aria-hidden="false" aria-labelledby="mondaypanel-label">
                    <h1><strong>MON-WED</strong></h1><h1>APR 9-11</h1>
                    <ul class="tabs " data-tabs id="monday-tabs">
                        <li class="tabs-title is-active" role="presentation"><a href="#mondaymorning" role="tab" aria-controls="mondaymorning" aria-selected="false" id="mondaymorning-label">Morning</a></li>
                        <li class="tabs-title" role="presentation"><a href="#mondayafternoon" role="tab" aria-controls="mondayafternoon" aria-selected="true" id="mondayafternoon-label">Afternoon</a></li>
                    </ul><!-- monday tabs ul -->
                    <div class="tabs-content" data-tabs-content="monday-tabs">
                        <div class="tabs-panel " id="mondaymorning" role="tabpanel" aria-hidden="true" aria-labelledby="mondaymorning-label">
                            <div class="container half ">
                                <p><strong>Open Meeting</strong><br>7:00 AM - 8:00 AM</p>
                                <p><strong>Registration</strong><br>7:30 AM - 5:00 PM</p>
                                <p><strong>Breakfast</strong><br>7:30 AM - 8:30 AM</p>
                                <p><strong>Exhibit Hall Open</strong><br>7:30 AM - 5:00 PM</p>
                            </div><!--container half -->
                            <div class="container half ">
                                <p><strong>Keynote</strong><br>8:30 AM - 10:00 AM</p>
                                <p><strong>Break/Networking</strong><br>10:00 AM - 10:30 AM</p>
                                <p><strong>Heroes Award</strong><br>10:00 AM - 10:15 AM</p>
                                <p><strong>Breakout Sessions</strong><br>10:30 AM - 12:00 PM</p>
                            </div><!--container half -->
                        </div><!--tabs panel monday morning -->
                        <div class="tabs-panel " id="mondayafternoon" role="tabpanel" aria-hidden="true" aria-labelledby="mondayafternoon-label">    
                            <div class="container half ">
                                <p><strong>Lunch(Ticketed Event)</strong><br>12:00 PM - 1:30 PM</p>
                                <p><strong>Breakout Sessions</strong><br>1:30 PM - 3:00 PM</p>
                                <p><strong>Break/Networking</strong><br>3:00 PM - 3:30 PM</p>
                                <p><strong>Keynote</strong><br>3:30 PM - 5:00 PM</p>
                                <p><strong>Open Meeting</strong><br>5:30 PM - 6:00 PM</p>
                            </div><!--container half -->
                            <div class="container half ">
                                <p><strong>Evening Event</strong><br>5:30 PM - 6:00 PM</p>
                            </div><!--container half -->
                        </div><!--tabs panel monday afternoon -->
                    </div><!--monday tabs content -->
                    </div><!--monday tabs panel -->
                <div class="tabs-panel " id="tuesdaypanel" role="tabpanel" aria-hidden="true" aria-labelledby="tuesdaypanel-label">
                    <h1><strong>FINAL DAY</strong></h1><h1>APR 12</h1>
                    <ul class="tabs " data-tabs id="tuesday-tabs">
                        <li class="tabs-title is-active" role="presentation"><a href="#tuesdaymorning" role="tab" aria-controls="tuesdaymorning" aria-selected="false" id="tuesdaymorning-label">Morning</a></li>
                    </ul><!-- tuesday tabs ul -->
                    <div class="tabs-content" data-tabs-content="tuesday-tabs">
                        <div class="tabs-panel " id="tuesdaymorning" role="tabpanel" aria-hidden="true" aria-labelledby="tuesdaymorning-label">
                            <div class="container half ">
                                <p><strong>Open Meeting</strong><br>6:30 AM - 7:30 AM</p>
                                <p><strong>Registration</strong><br>7:30 AM - 5:00 PM</p>
                                <p><strong>Breakfast</strong><br>7:30 AM - 8:30 AM</p>
                                <p><strong>Exhibit Hall Open</strong><br>7:30 AM - 5:00 PM</p>
                            </div><!--container half -->
                            <div class="container half ">
                                <p><strong>AM Keynote</strong><br>8:30 AM - 10:00 AM</p>
                                <p><strong>Break/Networking</strong><br>10:00 AM - 10:30 AM</p>
                                <p><strong>Closing Keynote</strong><br>10:30 AM - 12:00 PM</p>
                            </div><!--container half -->
                        </div> <!-- tabs panel tuesday morning -->
                        </div><!-- tuesday tabs content -->
            </div><!-- tuesday tabs panel -->
            
            </div><!--agenda tabs content -->
	</div><!-- end entry content -->	
    </div><!-- end inner  -->
</div><!-- end module wysiwyg -->