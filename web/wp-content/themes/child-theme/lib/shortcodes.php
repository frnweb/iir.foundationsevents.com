<?php

/* 
Custom Shortcodes
 */

//SVG MOC LOGO 
function moc_logo() {
  ob_start();
  get_template_part('parts/pieces/svg', 'fe-logo');
  return '<div class="fe-logo">'. ob_get_clean() .'</div>';
}
add_shortcode('moc_logo', 'moc_logo');

//SVG IIR RIBBON
function iir_ribbon() {
  ob_start();
  get_template_part('parts/pieces/svg', 'iir-ribbon');
  return '<div class="svg svg--shortcode">'. ob_get_clean() .'</div>';
}
add_shortcode('iir_ribbon', 'iir_ribbon');

//SVG IIR KEY
function iir_key() {
  ob_start();
  get_template_part('parts/pieces/svg', 'iir-key');
  return '<div class="svg svg--shortcode">'. ob_get_clean() .'</div>';
}
add_shortcode('iir_key', 'iir_key');

//SVG IIR NOTEBOOK
function iir_notebook() {
  ob_start();
  get_template_part('parts/pieces/svg', 'iir-notebook');
  return '<div class="svg svg--shortcode">'. ob_get_clean() .'</div>';
}
add_shortcode('iir_notebook', 'iir_notebook');

//SVG MOC AWARD
function moc_award() {
  ob_start();
  get_template_part('parts/pieces/svg', 'moc-award');
  return '<div class="svg svg--shortcode">'. ob_get_clean() .'</div>';
}
add_shortcode('moc_award', 'moc_award');

//// Adds Container Around Content with Class
//function div_container ( $atts, $content = null ) {
//	$specs = shortcode_atts( array(
//		'class'		=> '',
//		), $atts );
//	return '<div class="container ' . esc_attr($specs['class']) . ' ">' .  do_shortcode ( $content ) . '</div>';
//}
//                
//add_shortcode ('container', 'div_container' );