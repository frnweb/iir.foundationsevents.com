<!-- this is the mobile off canvas menu -->

<?php 

//Start defining variables for building the header
$navtype = get_field('nav_type', 'option');
$mobilenav = get_field('mobile_navtype', 'option');
$searchtype = get_field('search_type', 'option');
$dropdowntype = get_field('dropdown_type', 'option');


// OffCanvas Mobile Menu
if($mobilenav == 'offcanvas') { ?>

	<div class="off-canvas position-right" id="off-canvas" data-off-canvas data-position="right">
		<div id="offcanvas_search">
		<?php get_search_form(); ?>
	    </div><!-- end offcanvas search -->
		<?php joints_off_canvas_nav(); ?>
	    <div id="userbased-mobile">
	   <?php wp_nav_menu( array( 'theme_location' => 'user-links' ) ); ?>
	    </div>
	    <div class="offcanvas-phone">Confidential and Private  <span class="phone"><?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in off canvas mobile menu"]'); ?></span></div>
	</div>

<?php }


// Popover Mobile Menu
if($mobilenav == 'popover') {
?>
	<div id="popover" class="hide-for-large">
		<div class="mobile-menu">
			<?php joints_top_nav(); ?>
		</div><!-- /.mobile-menu -->
	</div><!-- /#popover -->
<?php } ?>