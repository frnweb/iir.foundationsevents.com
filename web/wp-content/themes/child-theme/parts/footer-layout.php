<?php
// Footer Layout

$navtype = get_field('nav_type', 'option');
$footerlogo = get_field('footer_logo', 'option');
$sitetitle = get_bloginfo('name');


// Footer Location
get_template_part('parts/footer', 'location'); ?>


<footer class="footer" role="contentinfo">            
    <div class="inner section">
        
        <div class="row footer-branding expanded large-collapse medium-collapse">
        	
        	<!-- Piece-Logo -->
        	<div class="columns large-3 medium-3">
            	<?php get_template_part('parts/pieces/footer', 'logo'); ?>
        	</div><!-- /.columns large-3 -->
        	
			<!-- Addons -->
           	<div class="columns large-9 medium-9">
            	<?php get_template_part('parts/pieces/footer', 'addons'); ?>
            </div><!-- /.columns large-4 -->
        
        </div> <!-- /.row -->
    </div><!-- /.inner section -->
        	
<?php 

echo do_shortcode('[frn_footer]'); ?>

</footer> <!-- end .footer -->